# **eRekrutacje**

Konifguracja dockerów pod aplikację demo Symfony w środdowisku mysql 8.0 php 7.4.


&nbsp;
## **Wymagania**
#

- Linux
- Docker v19.03.13 (tylko ta wersja chodzi w sieci UM)

&nbsp;
## **Instalacja**
#

Stwóż katalog, w którym chcesz mieć źródła projektu. Sklonuj projekt z Githuba.
~~~
$ mkdir SymfonyDocker
$ cd SymfonyDocker
$ git clone ...  .
~~~

&nbsp;
## **Uruchamienie z dockerem**
#

Aplikacje można uruchomić za pomocą dockera. Działa pod wersją dockera 19.03.13 (lub prawdopodobnie starszą, ale tylko ta działa w sieci UM). Do sterowania kontenerami zostały przygotowane skrypty bash'a. Aby uruchomić aplikację wystarczy w głównym katalogu projektu uruchomić:
~~~
$ ./start.sh
~~~
Po uruchomieniu skryptu należy chwilę poczekać aż kontenery się zbudują i wykonają wszystkie skrypty konfigurujące. Przez chwilę na stronie http://127.0.0.1:8080 może pojawiać się komunikat "502 Bad Gateway"

- Pod adresem **http://127.0.0.1:8080** będzie widoczna jest strona prjektu
- Pod adresem **http://127.0.0.1:8001** będzie widoczna jest strona phpmyadmin do bazy danych projektu
- Pod adresem **127.0.0.1:3309** będzie dotępna baza danych mysql 

Po uruchomieniu kontenerów lokalny użytkownik nie traci uprawnień do katalogów z bazą danych ani do katalogu źródeł.

&nbsp;
> Uruchomienie wymaga dostępu do sieci. W sieci UM nie zawsze jesteśmy autoryzowani na proxy. Wtedy w logach docker-composera pojawia się error łączenia do adresu w stylu: 
> ~~~
> php-fpm_1     |   The 'https://proxy5.bzmw.gov.pl/B0001D0000N0000N0001F0000S0000R0004/10.72.145.136/https://repo.packagist.org/packages.json' URL required authentication.  
> ~~~
> Aby temu zaradzić najlepiej wejść z przeglądarki na ten  adres z logu. Wyskoczy monit o autoryzacja (na windowsach zautoryzuje się automatycznie). Można też wejść z virtualnego windowsa na tym samym komputerze.

&nbsp;
### **Konfiguracja**

Konfigurtacja jest zawarta w pliku docker/.env

~~~
#docker/.env 

DATABASE_NAME=symfonyDB
DATABASE_USER=appuser
DATABASE_PASSWORD=apppassword
DATABASE_ROOT_PASSWORD=secret

APP_ENV=dev
APP_SECRET=24e17c47430bd2044a61c131c1cf6990

USER_ID=1000
GROUP_ID=1000
~~~

|Zmienna|Znaczenie|
| - | - |
| **DATABASE_NAME** |  Nazwa bazy danych używana przez aplikację  |
| **DATABASE_USER** | User, którym aplikacja loguje się do bazy danych  |
| **DATABASE_PASSWORD** | Hasło do bazy danych |
| **DATABASE_ROOT_PASSWORD** | Hasło roota do bazy danych |
| **APP_ENV** | Rodzaj środowiska, w którym jest uruchamiana aplikacja  |
| **USER_ID**  | ID użytkownika z systemu hosta, z którym uruchamiana jest aplikacja ( temu użytkownikowi będą przypisane uprawnienia do katalogów bazy danych i dynamicznych źródeł symfony ) |
| **GROUP_ID** | ID grupy tak jak wyżej. |

&nbsp;
### **Skrytpy sterujące**

Skrypty wykonujemy w głównym katalogu projektu:

|Skrypt| |
| - | - |
|**./start.sh** | skrypt uruchamiający kontenery z aplikacją eRekrutacje. Jeśli nie ma obrazów to je tworzy. Dodatkowo skrypt określa UID i GID użytkownika, który uruchamia środowisko kontenerowe i nadaje wartości zmiennym USER_ID i GROUP_ID  w pliku .env |
|**./stop.sh** | zatrzymuje kontenery.|  
|**./clear.sh** | Czyści pliki bazy danych i pliki wytworzone na skutek instalacji composera i yarna.| 
|**./dockerBash.sh** | Pozwala zalogować się do konsoli kontenera z symfony.| 



&nbsp;  
### **Przydatne polecenia**
Pokazuje na żywo log z dockera. Przydatne po uruchomieniu **start.sh** jeśli chcemy śledzić błędy i aktywność kontenerów:
~~~
    $ docker-compose logs -f
~~~  

W katalogu docker/ po wykonaniu **stop.sh**, jeśli chcemy przebudować kontenery, można uruchomić:

~~~
    $ docker-compose up --build -d
~~~  

Dostęp do konsoli dockera z php

~~~
    $ ddocker exec -it symfony_php-fpm_1  login localHostUser
~~~


Uruchamia proces bash na kontenerze. Dzięki temu mamy dostęp do terminala.  
ID - id danego kontenera z polecenia **docker ps**  

~~~
    $ docker ps
    $ docker exec -it [ID] /bin/bash
~~~

Wchodzimy do konsoli dockera i tam uruchamiamy różne komendy, które konfigurują projekt (symfony, yarn, doctrine)
   ~~~
   cd /var/www/docker/eRekrutacje
   ./dockerBash.sh
   composer install
   yarn install
   yarn encore dev
   yarn encore dev --watch
   bin/console doctrine:migrations:migrate
   bin/console cache:clear
   ... ect.
   ~~~


&nbsp;  
### **Łączenie się do bazy danych**

&nbsp;  
**phpmyadmin:** 

Pod adresem http://127.0.0.1:8001  

&nbsp;  
**bash/commandline:**
~~~
$ mysql -h 127.0.0.1 --port=3309  -u'root' -p'secret'
~~~

Hasło root jest zdefiniowane w zmiennej DATABASE_ROOT_PASSWORD w pliku docker/.env.  

&nbsp;  
**Za pomocą DBeaver:**  
  
Przy łączeniu do bazy poprzez DBeaver należy w zakładce **SSL** zaznaczyć **"use SSL"** i odhaczyć wszystkie opcje oprócz **"Allow public key retrieval"**.

&nbsp;  
### **Jak działać na projekcie symfony**

Niektóre rzeczy w projektach symfony trzeba wykonać z konsoli. Te uruchamiane polecaniami **symfony** lub **bin/console**. Ponieważ całe środowisko działa z kontenera, to te polecenia trzeba wykonywać z poziomu kontenera, a nie hosta. Żeby zalogować się do basha kontenera z symfony (php_fpm) należy wykonać skrypt **./dockerBash.sh** w katalogu głównym. 


&nbsp;  
### **Jak to działa**  

Aplikacja bazuje na pięciu obrazach dockera: 
- mysql:8.0
- nginx:alpine
- php:7.4-fpm-alpine
- phpmyadmin
- composer


Po uruchomieniu start.sh:
- Ustawiane są zmenne USER_ID i GROUP_ID 
- budują się kontenery
- wykonuje się skrypt tworzący aplikację demo symfony w katalogu src/symfony
- uruchamiany jest composer i yarn.
- zmieniany jest właściciel:grupa katalogów database/data/* i /src/* na USER_ID:GROUP_ID ( (zmienne z pliku docker/.env lub UID:GUID użytkownika uruchamiającego skrypt ./start.sh)
 

&nbsp;  
### **Odnośniki**  

https://dev.to/martinpham/symfony-5-development-with-docker-4hj8  
https://jtreminio.com/blog/running-docker-containers-as-current-host-user/  
https://bitbucket.org/BuczkoMarcin/docker-for-symfony5/src/master/
