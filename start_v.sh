#!/usr/bin/env bash

cd docker
sed -i "s/^USER_ID=.*$/USER_ID=$(id -u)/g; s/^GROUP_ID=.*$/GROUP_ID=$(id -u)/g;" .env
docker-compose up --build
