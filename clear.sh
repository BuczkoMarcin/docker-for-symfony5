#!/usr/bin/env bash

sudo rm -rf docker/database/data/*
sudo rm -rf src/symfony/vendor
sudo rm -rf src/symfony/var
sudo rm -rf src/symfony/node_modules
sudo rm -rf src/symfony/.cache
sudo rm -rf src/symfony/.composer
sudo rm -rf src/symfony/.yarn


