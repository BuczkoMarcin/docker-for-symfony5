# php-fpm/Dockerfile

FROM php:7.4-fpm-alpine

ARG USER_ID
ARG GROUP_ID

COPY wait-for-it.sh /usr/bin/wait-for-it
RUN chmod +x /usr/bin/wait-for-it
RUN apk --update --no-cache add git bash libxml2-dev openldap-dev freetype-dev libjpeg-turbo-dev libpng-dev curl-dev oniguruma-dev libzip-dev ca-certificates yarn wget
RUN docker-php-ext-install pdo_mysql mbstring xml ldap gd curl json zip
COPY --from=composer /usr/bin/composer /usr/bin/composer
WORKDIR /var/www
COPY ca-certificates/ /usr/local/share/ca-certificates/
RUN update-ca-certificates
RUN if [[ ''$(getent passwd ${USER_ID:-0}) == '' ]]; then \
        addgroup -g ${GROUP_ID:-0} localHostUser &&\
         adduser -G localHostUser -D -h /var/www -s /bin/bash -u ${USER_ID:-0} localHostUser; \
     fi
CMD          wget https://get.symfony.com/cli/installer -O - | bash; \
             mv /root/.symfony/bin/symfony /usr/local/bin/symfony; \
             cat installDemo.sh | bash; \
             cd symfony; \
             composer install --no-interaction; \
             yarn install; \
             yarn encore dev; \
             bin/console doctrine:schema:create; \
             bin/console doctrine:fixtures:load --no-interaction; \
             wait-for-it database:3306 -- bin/console doctrine:migrations:migrate ; \
             bin/console cache:clear; \
             if id -u localHostUser; then \
                chown -R  localHostUser:localHostUser ./; \
                passwd localHostUser -d ''; \
             fi; \
             php-fpm
EXPOSE 9000
