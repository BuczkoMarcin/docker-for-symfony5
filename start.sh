#!/usr/bin/env bash

chmod a+x src
cd docker
sed -i "s/^USER_ID=.*$/USER_ID=$(id -u)/g; s/^GROUP_ID=.*$/GROUP_ID=$(id -g)/g;" .env
docker-compose up --build -d
docker-compose logs -f
