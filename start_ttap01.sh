#!/usr/bin/env bash


cd docker
sudo sed -i "s/^USER_ID=.*$/USER_ID=$(id -u dockerERekrutacje)/g; s/^GROUP_ID=.*$/GROUP_ID=$(id -g dockerERekrutacje)/g;" .env
sudo /usr/local/bin/docker-compose up --build -d
